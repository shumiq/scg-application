new Vue({
    el: '#xyz',
    data: function () {
        return {
            request: null,
            response: null
        }
    },
    methods: {
        xyz: function (params) {
            var url = "http://localhost:3000/api/xyz/" + params
            this.request = "GET: " + url
            this.response = "loading..."
            axios
                .get(url)
                .then(response => {
                    var res = "status: " + response.status + "\n"
                    res += "data: " + JSON.stringify(response.data, null, 2)
                    this.response = res
                })
        }
    }
})

new Vue({
    el: '#restaurants',
    data: function () {
        return {
            request: null,
            response: null,
            table: null
        }
    },
    methods: {
        restaurants: function () {
            var url = "http://localhost:3000/api/restaurants/"
            this.request = "GET: " + url
            this.response = "loading..."
            this.table = null
            axios
                .get(url)
                .then(response => {
                    var res = "status: " + response.status + "\n"
                    res += "data: " + JSON.stringify(response.data, null, 2)
                    this.response = res
                    var tableData = ""
                    for (var i in response.data.results) {
                        var row = "<tr>"
                        row += "<td>" + response.data.results[i].name + "</td>"
                        row += "<td>" + response.data.results[i].formatted_address + "</td>"
                        row += "<td>" + response.data.results[i].rating + "</td>"
                        row += "</tr>"
                        tableData += row
                    }
                    this.table = tableData
                })
        }
    }
})

new Vue({
    el: '#broadcast',
    data: function () {
        return {
            message: null,
            request: null,
            response: null
        }
    },
    methods: {
        broadcast: function () {
            var url = "http://localhost:3000/api/broadcast/" + this.message
            this.request = "GET: " + url
            this.response = "loading..."
            axios
                .get(url)
                .then(response => {
                    var res = "status: " + response.status + "\n"
                    res += "data: " + JSON.stringify(response.data, null, 2)
                    this.response = res
                    this.message = null
                })
        }
    }
})