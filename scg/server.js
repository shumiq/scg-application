const express = require('express')
const request = require('request-promise')
const app = express()

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html')
})

app.get('/cv', (req, res) => {
    res.sendFile(__dirname + '/cv.html')
})

app.get('/app.js', (req, res) => {
    res.sendFile(__dirname + '/app.js')
})


////////////////////////////////////////////////////////////////////////////////////
// X, 5, 9, 15, 23, Y, Z - Please create a new function for finding X, Y, Z value //
////////////////////////////////////////////////////////////////////////////////////

app.get('/api/xyz', (req, res) => {
    res.json({
        question: ['x', 5, 9, 15, 23, 'y', 'z'],
        answer: [3, 5, 9, 15, 23, 33, 45]
    })
})

app.get('/api/xyz/:expr', (req, res) => {
    var answers = {
        x: 3,
        y: 33,
        z: 45
    }
    var answer = 'not found'
    if (req.params.expr == 'all') answer = answers
    if (answers[req.params.expr] != null) answer = answers[req.params.expr]
    res.json({
        expr: req.params.expr,
        answer: answer
    })
})


////////////////////////////////////////////////////////////////////////////////
// Please use "Place search|Place API(by Google)" for finding all restaurants //
// in Bangsue area and show result by JSON                                    //
////////////////////////////////////////////////////////////////////////////////

var google_api_key = 'AIzaSyDCCuQjK1zo4dMWG6m1F2F0OWTec-A3Ef0'
var google_api = 'https://maps.googleapis.com/maps/api/place/textsearch/json'
var google_api_query = 'restaurants+in+Bangsue'
var google_api_request = google_api + '?query=' + google_api_query + '&key=' + google_api_key

app.get('/api/restaurants', (req, res) => {
    request({
        method: 'GET',
        uri: google_api_request
    }).then((resp) => {
        res.json(JSON.parse(resp))
    }).catch((error) => {
        res.json(JSON.parse(error))
    })
})

////////////////////////////////////////////////////////////////////////////////////
// Please create one small project for Line messaging API(Up to you)              //
// contain a flow diagram, your code, and database.                               //
////////////////////////////////////////////////////////////////////////////////////

// channel_id = '@318vbzgq'

var line_api = 'https://api.line.me/v2/bot/message'
var line_header = {
    'Content-Type': 'application/json',
    'Authorization': 'Bearer jugUgE8YRo5rWpD0LovyVfD10osisU3X6JF5aI4nZD2eHzsNwyzSjG/ZUtGqUt3mdDF/Ns12xFqqJ5CEmahf5ZUVl4LPHMHBMbvA/P0rjThVnzivLFf+ZnAn1rhI7Y8OY32+sm6vRHAADrHOnjWXUwdB04t89/1O/w1cDnyilFU='
}

app.get('/api/broadcast/:msg', (req, res) => {
    request({
        method: 'POST',
        uri: line_api + '/broadcast',
        headers: line_header,
        body: JSON.stringify({
            messages: [{
                type: 'text',
                text: req.params.msg
            }]
        })
    }).then(() => {
        res.json({
            status: 200,
            message: req.params.msg
        })
    }).catch((error) => {
        res.json({
            status: 500,
            error: error,
            message: req.params.msg
        })
    })
})

////////////////////////////////////////////////////////////////////////////////////

app.listen(3000, () => {
    console.log('Start server at port 3000.')
})