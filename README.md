SCG's Job Application
=========================

This project is made by Santiphap Watcharasukchit. The purpose of this project is for applying SCG's job.

***

Requirements:
--------------

In order to use the application, the following modules are needed to be installed.

    nodejs


Usage:
------

Start server
	
	node scg/server.js

Web Interface

    http://localhost:3000/

Find X, Y, Z value

    GET: http://localhost:3000/api/xyz
    GET: http://localhost:3000/api/xyz/all
    GET: http://localhost:3000/api/xyz/{x|y|z}

Find all restaurants in Bangsue area

    GET: http://localhost:3000/api/restaurants

Broadcast message to MyApp channel (line id: @318vbzgq)

    GET: http://localhost:3000/api/broadcast/{message}
    
Authors:
--------

    Santiphap Watcharasukchit santiphap.wat@gmail.com
